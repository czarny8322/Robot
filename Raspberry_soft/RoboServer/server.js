// https://scotch.io/tutorials/build-a-restful-api-using-node-and-express-4

// server.js

// BASE SETUP
// =============================================================================

// call the packages we need

//var http = require("http");
var express = require('express');        // call express
var app = express();                 // define our app using express
var bodyParser = require('body-parser');
var path = require("path");




app.use(express.static(__dirname + '/views'));

var wpi = require('wiring-pi');
wpi.setup('gpio');

initDrive();
var fd = initHead();


//var fs = require("fs");
//var body = fs.readFileSync('client.html');

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());




// HOME INDEX.HTML
// =============================================================================

app.get('/', function (req, res) {
    res.sendFile('/views/index.html', { root: __dirname });
    //It will find and locate index.html from View or Scripts
    next();
});




// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// middleware to use for all requests
router.use(function (req, res, next) {
    // do logging        

    next(); // make sure we go to the next routes and don't stop here
});


router.route('/drive')

    .get(function (req, res) {
        res.json({ message: '/get' });
    })
    .post(function (req, res) {

        var direction = req.body.direction;

        switch (direction) {
            case 'up':
                goUp();
                console.log('direction : ' + 'up');
                res.json({ direction: 'up' });
                break;
            case 'down':
                goDown();
                console.log('direction : ' + 'down');
                res.json({ direction: 'down' });
                break;
            case 'left':
                goLeft();
                console.log('direction : ' + 'left');
                res.json({ direction: 'left' });
                break;
            case 'right':
                goRight();
                console.log('direction : ' + 'right');
                res.json({ direction: 'right' });
                break;
            case 'stop_drive':
                goStop();
                console.log('direction : ' + 'stop_drive');
                res.json({ direction: 'stop_drive' });
                break;


            /// ================   HEAD   ============================== ///

            case 'head_goUp':
                head_goUp();
                console.log('direction : ' + 'head_goUp');
                res.json({ direction: 'head_goUp' });
                break;
            case 'head_goDown':
                head_goDown();
                console.log('direction : ' + 'head_goDown');
                res.json({ direction: 'head_goDown' });
                break;
            case 'head_goLeft':
                head_goLeft();
                console.log('direction : ' + 'head_goLeft');
                res.json({ direction: 'head_goLeft' });
                break;
            case 'head_goRight':
                head_goRight();
                console.log('direction : ' + 'head_goRight');
                res.json({ direction: 'head_goRight' });
                break;

            case 'head_stop':
                head_stop();
                console.log('direction : ' + 'head_stop');
                res.json({ direction: 'head_stop' });
                break;

            case 'head_turnON':
                head_turnON();
                console.log('head_turnON');
                res.json({ direction: 'head_turnON' });
                break;
            case 'head_turnOFF':
                head_turnOFF();
                console.log('head_turnOFF');
                res.json({ direction: 'head_turnOFF' });
                break;
        }
    });


router.route('/gpio')
    // get all gpio statuses (accessed at GET http://localhost:8080/gpio)   
    .get(function (req, res) {
        res.json({ message: '/gpio/get' });
    });

router.route('/direction/:direction')

    // get the bear with that id (accessed at GET http://localhost:8080/direction/:direction')
    .post(function (req, res) {

    });

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/', router);

// START THE SERVER
// =============================================================================

app.listen(9000);
console.log('Server running at http://127.0.0.1:9000/');



//Pin Connection Rpi & DC//

//gpio 22 --> right DC --> down
//gpio 10 --> right DC --> up
//gpio 9  --> left  DC --> up
//gpio 11 --> left  DC --> down

var rightDC_down = 22;
var rightDC_up = 10;
var leftDC_up = 9;
var leftDC_down = 11;


function goUp() {
    wpi.digitalWrite(rightDC_up, wpi.HIGH);
    wpi.digitalWrite(leftDC_up, wpi.HIGH);
};

function goDown() {
    wpi.digitalWrite(rightDC_down, wpi.HIGH);
    wpi.digitalWrite(leftDC_down, wpi.HIGH);
};

function goRight() {
    wpi.digitalWrite(rightDC_down, wpi.HIGH);
    wpi.digitalWrite(leftDC_up, wpi.HIGH);
};

function goLeft() {
    wpi.digitalWrite(rightDC_up, wpi.HIGH);
    wpi.digitalWrite(leftDC_down, wpi.HIGH);
};

function goStop() {
    wpi.digitalWrite(rightDC_down, wpi.LOW);
    wpi.digitalWrite(rightDC_up, wpi.LOW);
    wpi.digitalWrite(leftDC_up, wpi.LOW);
    wpi.digitalWrite(leftDC_down, wpi.LOW);
};

function initDrive() {

    // set gpio pin to output mode //
    wpi.pinMode(22, 1);
    wpi.pinMode(10, 1);
    wpi.pinMode(9, 1);
    wpi.pinMode(11, 1);

    console.log('drive GPIO has been loaded');
};

//Pin Connection Rpi & Arduino - HEAD//



function initHead() {
    console.log('Connect to head - I2C');
    var checkI2c = wpi.wiringPiI2CSetup(0x08);
    if (checkI2c < 0) {
        console.log('problem with I2C');
    } else {
        console.log('I2c - connected');
    }
    return checkI2c;
};

function head_goUp() {
    wpi.wiringPiI2CWrite(fd, 0x01);
    console.log('head_goUp');
};
function head_goDown() {
    console.log('head_goDown');
    wpi.wiringPiI2CWrite(fd, 0x02);
};
function head_goLeft() {
    console.log('head_goLeft');
    wpi.wiringPiI2CWrite(fd, 0x03);
};
function head_goRight() {
    console.log('head_goRight');
    wpi.wiringPiI2CWrite(fd, 0x05);
};
function head_stop() {
    console.log('head_stop');
    wpi.wiringPiI2CWrite(fd, 0x00);
};
function head_turnON() {
    console.log('head_turnON');
    wpi.wiringPiI2CWrite(fd, 0x08);
};
function head_turnOFF() {
    console.log('head_turnOFF');
    wpi.wiringPiI2CWrite(fd, 0x09);
};

