// Wire Slave Receiver
// by Nicholas Zambetti <http://www.zambetti.com>

// Demonstrates use of the Wire library
// Receives data as an I2C/TWI slave device
// Refer to the "Wire Master Writer" example for use with this

// Created 29 March 2006

// This example code is in the public domain.


#include <Wire.h>
#include <Servo.h>

  Servo head_horizontal_servo;  
  Servo head_vertical_servo;
  
  int X_position = 75;
  int Y_position = 59;
  int headDirection = 0;

// I2C - direction value //
  int UP = 1;
  int DOWN = 2;
  int RIGHT = 5;
  int LEFT = 3;
  int STOP = 0;
// ------- //  

void setup() {
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600);           // start serial for output
  
  //head_horizontal_servo.attach(10);
  head_horizontal_servo.write(X_position);
  
  //head_vertical_servo.attach(9);
  head_vertical_servo.write(Y_position);
}

void loop() {

if(headDirection == UP){
  Serial.println("UP");
            
             for(int pos = Y_position; pos > 0; pos--)  // goes from 0 degrees to 90 degrees
              {                                  // in steps of  degree
                    head_vertical_servo.write(pos);              // tell servo to go to position in variable 'pos'
                    Serial.println(pos);
                             
                 Y_position = pos;
                 delay(20);
                 
                 if(headDirection == STOP)
                 return;
              }
}

if(headDirection == DOWN){
  Serial.println("DOWN");
            
             for(int pos = Y_position; pos < 90; pos++)  // goes from 0 degrees to 90 degrees
              {                                  
                    head_vertical_servo.write(pos);              // tell servo to go to position in variable 'pos'
                    Serial.println(pos);
                             
                 Y_position = pos;
                 delay(20);
                 
                 if(headDirection == STOP)
                 return;
              }
}

if(headDirection == RIGHT){
  Serial.println("RIGHT");
            
             for(int pos = X_position; pos > 0; pos--)  // goes from 0 degrees to 90 degrees
              {                                  // in steps of  degree
                    head_horizontal_servo.write(pos);              // tell servo to go to position in variable 'pos'
                    Serial.println(pos);
                             
                 X_position = pos;
                 delay(20);
                 
                 if(headDirection == STOP)
                 return;
              }
}

if(headDirection == LEFT){
  Serial.println("LEFT");
            
             for(int pos = X_position; pos < 180; pos++)  // goes from 0 degrees to 90 degrees
              {                                  // in steps of  degree
                    head_horizontal_servo.write(pos);              // tell servo to go to position in variable 'pos'
                    Serial.println(pos);
                             
                 X_position = pos;
                 delay(20);
                 
                 if(headDirection == STOP)
                 return;
              }
             
}
}
// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
  while (1 < Wire.available()) { // loop through all but the last
    //char c = Wire.read(); // receive byte as a character
    //Serial.print(c);         // print the character
  }
  headDirection = Wire.read();    // receive byte as an integer
  Serial.println(headDirection);         // print the integer

  if(headDirection == 9){
    Serial.println("turnOFF");
    head_horizontal_servo.detach();
       head_vertical_servo.detach();
  }

  if(headDirection == 8){
    Serial.println("turnON");
    head_horizontal_servo.attach(10);
    head_vertical_servo.attach(9);
  }
  
  
}
