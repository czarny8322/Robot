#include <Servo.h>

Servo head_horizontal_servo;  
Servo head_vertical_servo;

int horizontal_up = 5;
int horizontal_down = 6;
int position_horizontal = 0;


int vartical_left = 3;
int vartical_right = 4;
int position_vertical = 0;


void setup()
{
  Serial.begin(9600);
  
  //head_horizontal_servo.attach(9); 
  head_horizontal_servo.write(45);  
  pinMode(horizontal_up, INPUT);
  pinMode(horizontal_down, INPUT);


  //head_vertical_servo.attach(10); 
  head_vertical_servo.write(90);
  pinMode(vartical_left, INPUT);
  pinMode(vartical_right, INPUT);
   
}

void loop()
{
 
  
  if (digitalRead(horizontal_up) == HIGH)
  for(int pos = position_horizontal; pos < 90; pos++)  // goes from 0 degrees to 90 degrees
  {                                  // in steps of  degree
        head_horizontal_servo.write(pos);              // tell servo to go to position in variable 'pos'
        Serial.print("UP: ");                              // waits 1s for the servo to reach the position
        Serial.println(pos);
                 
     position_horizontal = pos;
     delay(20);

     if(!digitalRead(horizontal_up) == HIGH)
     return;
  }
  
  if (digitalRead(horizontal_down) == HIGH) 
  for(int pos = position_horizontal ; pos>=0; pos--)     
  {                               
     head_horizontal_servo.write(pos);  
     Serial.print("DOWN: ");  
     Serial.println(pos); 
      
     position_horizontal = pos;
     delay(20);

     if (!digitalRead(horizontal_down) == HIGH)
     return; 

  }



//////////////////////////////////

 if (digitalRead(vartical_left) == HIGH)
  for(int pos = position_vertical; pos < 180; pos++)  // goes from 0 degrees to 90 degrees
  {                                  // in steps of  degree
        head_vertical_servo.write(pos);              // tell servo to go to position in variable 'pos'
        Serial.print("Left: ");                              // waits 1s for the servo to reach the position
        Serial.println(pos);
                 
     position_vertical = pos;
     delay(20);

     if(!digitalRead(vartical_left) == HIGH)
     return;
  }
  
  if (digitalRead(vartical_right) == HIGH) 
  for(int pos = position_vertical ; pos>=0; pos--)     
  {                               
     head_vertical_servo.write(pos);  
     Serial.print("right: ");  
     Serial.println(pos); 
      
     position_vertical = pos;
     delay(20);

     if (!digitalRead(vartical_right) == HIGH)
     return; 

  }

  
 
}
